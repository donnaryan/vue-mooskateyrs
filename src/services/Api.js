import axios from 'axios'
import firebase from 'firebase'

// Initialize Firebase - Dev
const config = {
  apiKey: "AIzaSyB1DCR1xZEC2mCcm-2cmfJ2TfzJhLi49eY",
  authDomain: "mookatyers-dev.firebaseapp.com",
  databaseURL: "https://mookatyers-dev.firebaseio.com",
  projectId: "mookatyers-dev",
  storageBucket: "mookatyers-dev.appspot.com",
  messagingSenderId: "43533552688"
};
// Initialize Firebase - Prod
// var config = {
//   apiKey: "AIzaSyBjLtt2LTCHFpusJOFlkgRqIb9erxyk5I0",
//   authDomain: "mooskat-eyrs.firebaseapp.com",
//   databaseURL: "https://mooskat-eyrs.firebaseio.com",
//   projectId: "mooskat-eyrs",
//   storageBucket: "mooskat-eyrs.appspot.com",
//   messagingSenderId: "1098540931136"
// };
firebase.initializeApp(config);

const API = axios.create({
  baseURL: config.databaseURL
})
const EXT = `.json`

// ---- Get Data ----
const getInternalCoins = () => {
  return API.get(`internal_coins.json`)
}
const getInternalTransactionTypes = () => {
  return API.get(`internal_transaction_type.json`)
}
const getCoins = () => {
  return API.get(`coins.json`)
}
const getLedgerEntries = () => {
  return API.get(`ledger` + EXT)
}
const getItemEntries = () => {
  return API.get(`items` + EXT)
}
const getLogEntries = () => {
  return API.get(`logs` + EXT)
}

// ---- Add Data
const addLedgerEntry = (data) => {
  return API.post(`ledger` + EXT, data)
}
const addItemEntry = (data) => {
  return API.post(`items` + EXT, data)
}
const addLogEntry = (data) => {
  return API.post(`logs` + EXT, data)
}

// ---- Update Data
const updateTotalQty = (data) => {
  return API.put(`coins/` + data.key + '/qty' + EXT, data.qty )
}
const updateItemEntry = (data) => {
  return API.put(`items/` + data.key + '/' + EXT, data)
}

// ---- Delete Data
const delLedgerEntry = (data) => {
  return API.delete(`ledger/` + data.key + `/.json`)
}
const delItemEntry = (data) => {
  return API.delete(`items/` + data.key + `/.json`)
}

export default {
  getInternalCoins,
  getInternalTransactionTypes,
  getCoins,
  
  updateTotalQty,

  getLedgerEntries,
  addLedgerEntry,
  delLedgerEntry,

  getItemEntries,
  addItemEntry,
  updateItemEntry,
  delItemEntry,

  getLogEntries,
  addLogEntry
}
