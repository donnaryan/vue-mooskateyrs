import Vue from 'vue'
import Vuex from 'vuex'
// modules
import internals from './internals'
import ledger from './ledger/index'
import items from './items/index'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    internals,
    ledger,
    items
  }
})
