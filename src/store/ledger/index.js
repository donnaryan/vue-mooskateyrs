// import API info
import API from '../../services/Api'

// initial state
const state = {
  entries: [],
  totals: {
    pp: { qty: 0, name: null },
    gp: { qty: 0, name: null },
    ep: { qty: 0, name: null },
    sp: { qty: 0, name: null },
    cp: { qty: 0, name: null }
  }
}

// getters
const getters = {
  currentEntries: (state) => {
    return state.entries
  },
  currentTotal: (state) => {
    return state.totals
  }
}

// action
const actions = {
  fetchEntries ({ commit }) {
    const response = API.getLedgerEntries()
    response
      .then((res) => {
        commit('setEntries', res.data)
      })
      .catch((err) => {
        console.log(`Store > Ledger > fetchAll > error:`, err)
      })
  },
  fetchTotal ({ commit }) {
    const response = API.getCoins()
    response.then((res) => {
      commit('setTotal', res.data)
    })
      .catch((err) => {
        console.log(err)
      })
  },
  addEntry ({ commit, dispatch }, item) {
    const response = API.addLedgerEntry(item)
    response
      .then((res) => {
        item.key = res.data.name
        item.from = 'add'
        commit('addEntry', item)
        const coinItem = { coin: item.coin, qty: item.qty, type: item.type }
        dispatch('updateTotals', coinItem, 'add')
        dispatch('internals/addLogEntry',
          {
            timestamp: item.date,
            entry: `[Ledger] Added ${item.qty} ${item.coin} because ${item.type} - Note: ${item.description}`
          },
          { root: true }
        )
      })
      .catch((err) => {
        console.log(`Store > Ledger > addEntry > error:`, err)
      })
  },
  updateTotals ({ commit, state, rootGetters }, item) {
    const ledgerTypes = rootGetters['internals/getLedgerTypes']

    // figure out the updated coins so we can update state and database
    let newTotal = 0
    let type = ledgerTypes[item.type].type

    if (item.from === 'del') {
      if (type === 'add') {
        type = 'sub'
      } else {
        type = 'add'
      }
    }

    switch (type) {
      case 'sub':
        newTotal = state.totals[item.coin].qty - parseInt(item.qty)
        break
      case 'add':
      default:
        newTotal = state.totals[item.coin].qty + parseInt(item.qty)
    }
    commit('totalUpdate', { key: item.coin, qty: newTotal })

    const response = API.updateTotalQty({ key: item.coin, qty: newTotal })
    response
      .then((res) => {})
      .catch((err) => {
        console.log(`Store > Ledger > updateTotals > error: `, err)
      })
  },
  deleteEntry ({ commit, dispatch }, item) {
    const response = API.delLedgerEntry(item)
    response
      .then((res) => {
        item.from = 'del'
        commit('delEntry', item)
        
        const coinItem = { 
          coin: item.coin,
          qty: item.qty,
          type: item.type,
          from: 'del'
        }
        dispatch('updateTotals', coinItem)
        // Set current date/time for log entry
        const now = new Date(Date.now()).toISOString()
        const nowBits = now.split('T')
        dispatch('internals/addLogEntry',
          {
            timestamp: nowBits[0],
            entry: `[Ledger] Removed entry ` + JSON.stringify(item)
          },
          { root: true })
      })
      .catch((err) => {
        console.log(`Store > Ledger > deleteEntry > error:`, err)
      })
  }
}
// mutations
const mutations = {
  setEntries (state, data) {
    var newEntries = Object.keys(data).map(key => {
      const newObj = data[key]
      newObj.key = key
      return newObj
    })
    state.entries = newEntries
  },
  setTotal (state, data) {
    state.totals = data
  },
  addEntry (state, data) {
    const currEntries = state.entries
    currEntries.push(data)
    state.entries = currEntries
  },
  totalUpdate (state, data) {
    console.log('Store > Ledger > updateTotals > data: ', data)
    state.totals[data.key].qty = data.qty
  },
  delEntry (state, data) {
    var newEntries = state.entries.filter((entry) => {
      return entry.key !== data.key
    })
    state.entries = newEntries
  }
}
// export
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
