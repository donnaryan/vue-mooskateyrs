// import API info
import API from '../../services/Api'

// initial state
const state = {
  entries: [],
  fetching: false
}

// getters
const getters = {
  currentEntries: (state) => {
    return state.entries
  }
}

// action
const actions = {
  fetchEntries ({ commit }) {
    commit('setFetch', true)
    const response = API.getItemEntries()
    response
      .then((res) => {
        if (res.data === null) {
          // no error, just nothing in the database
        } else {
          commit('setEntries', res.data)
        }
      })
      .catch((err) => {
        console.log('Store > Items > fetchAll > error: ', err)
      })
    commit('setFetch', false)
  },
  addItemEntry ({ commit, dispatch }, item) {
    commit('setFetch', true)
    const response = API.addItemEntry(item)
    response
      .then((res) => {
        item.key = res.data.name
        commit('addEntry', item)
        dispatch('internals/addLogEntry',
          {
            timestamp: item.date,
            entry: `[Item] Added ${item.qty} ${item.name} worth ${item.worth} ${item.coin} from location: ${item.location}`
          },
          { root: true }
        )
      })
      .catch((err) => {
        console.log('Store > Items > addItemEntry > error: ', err)
      })
    commit('setFetch', false)
  },
  updateItemEntry ({ commit }, item ) {
    commit('setFetch', true)
    const response = API.updateItemEntry(item)
    response
      .then((res) => {
        commit('updateEntry', item)
      })
      .catch((err) => {
        console.log('Store > Items > updateItemEntry > error: ', err)
      })
    commit('setFetch', false)
  },
  sellItemEntry ({ commit, dispatch }, item) {
    // commit('setFetch', true)
    // Figure out price at quarter
    const sellDiscount = 0.25
    const sellTotal = (item.worth * sellDiscount) * item.qty

    // TODO: Do we have a full integer or do we have a floater?
    // if (Number(sellTotal) === sellTotal && sellTotal % 1 !== 0) {
    //   console.log('[sellItemEntry] We need to figure coin conversion', sellTotal)
    // }

    // Get the Date
    const now = new Date(Date.now()).toISOString()
    const nowBits = now.split('T')

    // Add item to the ledger as 'Sold Inventory' (inv)
    const newLedgerEntry = {
      date: nowBits[0],
      type: 'inv',
      coin: item.coin,
      qty: sellTotal,
      description: `[Sold Inventory] ${item.qty} ${item.name}`
    }
    const newLogEntry = {
      timestamp: nowBits[0],
      entry: `[Sold Inventory] ${item.qty} ${item.name} sold for ${sellTotal} ${item.coin}`
    }
    const response = API.delItemEntry(item)
    response
      .then((res) => {
        dispatch('ledger/addEntry', newLedgerEntry, { root: true })
        dispatch('internals/addLogEntry', newLogEntry, { root: true })
        commit('delEntry', item)
      })
      .catch((err) => {
        console.log('Store > Items > sellItemEntry > Error:', err)
      })
  },
  deleteItemEntry ({ commit, dispatch }, item) {
    console.log('deleteItemEntry')
    commit('setFetch', true)
    const response = API.delItemEntry(item)
    response
      .then((res) => {
        commit('delEntry', item)
        // set current date for log entry
        const now = new Date(Date.now()).toISOString()
        const nowBits = now.split('T')
        dispatch('internals/addLogEntry',
          {
            timestamp: nowBits[0],
            entry: `[Item] Gave Away: ` + JSON.stringify(item)
          },
          { root: true }
        )
      })
      .catch((err) => {
        console.log('Store > Items > deleteItemEntry > Error: ', err)
      })
    commit('setFetch', false)
  }
}

// mutations
const mutations = {
  setFetch  (state, data) {
    state.fetching = data
  },
  setEntries (state, data) {
    const newEntries = Object.keys(data).map(key => {
      const newObj = data[key]
      newObj.key = key
      return newObj
    })
    state.entries = newEntries
  },
  addEntry (state, data) {
    const currEntries = state.entries
    currEntries.push(data)
    state.entries = currEntries
  },
  updateEntry (state, data ) {
    const entries = state.entries
    for (let entry in entries) {
      if (entry.key === data.key) {
        entry = data
      }
    }
    state.entries = entries
  },
  delEntry (state, data) {
    const entries = state.entries.filter((entry) => {
      return entry.key !== data.key
    })
    state.entries = entries
  }
}

// export
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
