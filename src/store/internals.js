// import api info
import API from '../services/Api'

// initial state
const state = {
  coinTypes: [],
  ledgerTypes: [],
  logEntries: []
}

// getters
const getters = {
  getCoinTypes: (state) => {
    return { ...state.coinTypes }
  },
  getLedgerTypes: (state) => {
    return { ...state.ledgerTypes }
  },
  getLogEntries: (state) => {
    return { ...state.logEntries }
  }
}

// actions
const actions = {
  fetchCoinTypes ({ commit }) {
    const response = API.getInternalCoins()
    response
      .then((res) => {
        commit('setCoinTypes', res.data)
      })
      .catch((err) => {
        console.log(err)
      })
  },
  fetchLedgerTypes ({ commit }) {
    const response = API.getInternalTransactionTypes()
    response
      .then((res) => {
        commit('setLedgerTypes', res.data)
      })
      .catch((err) => {
        console.log(err)
      })
  },
  fetchLogEntries ({ commit }) {
    const response = API.getLogEntries()
    response
      .then((res) => {
        console.log('[fetchLogEntries]', res.data)
        commit('setLogEntries', res.data)
      })
      .catch((err) => {
        console.log(err)
      })
  },
  addLogEntry ({ commit }, item) {
    console.log('[internals] item', item)
    const response = API.addLogEntry(item)
    response
      .then((res) => {
        commit('updateLogEntries', res.data)
      })
      .catch((err) => {
        console.log(err)
      })
  }
}

// mutations
const mutations = {
  setCoinTypes (state, data)  {
    var newTypes = Object.keys(data).map(key => {
      return { value: key, text: data[key] }
    })
    state.coinTypes = newTypes
  },
  setLedgerTypes (state, data) {
    state.ledgerTypes = data
  },
  setLogEntries (state, data) {
    state.logEntries = data
  },
  updateLogEntries (state, data ) {
    const newLogEntries = state.logEntries
    newLogEntries.push(data)
    state.logEntries = newLogEntries
  }
}

// export
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
