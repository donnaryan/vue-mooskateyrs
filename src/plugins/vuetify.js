import Vue from 'vue'
import Vuetify from 'vuetify'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/pro-regular-svg-icons'
import { fal } from '@fortawesome/pro-light-svg-icons'

Vue.component('font-awesome-icon', FontAwesomeIcon)
library.add(fas)
library.add(far)
library.add(fal)

Vue.use(Vuetify, {
  iconfont: 'faSvg',
  theme: {
    "primary": "#4caf50",
    "secondary": "#4caf50",
    "accent": "#82B1FF",
    "error": "#FF5252",
    "info": "#2196F3",
    "success": "#4CAF50",
    "warning": "#FB8C00"
  }
})
